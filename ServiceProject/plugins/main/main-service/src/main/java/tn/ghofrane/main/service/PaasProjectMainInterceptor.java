package tn.ghofrane.main.service;

import net.vpc.upa.config.*;
import java.util.logging.Logger;

/**
 * Application UPA interceptor for Main module.
 * 
 * @author Ghofrane
 */
@Callback
public class PaasProjectMainInterceptor {
    private static final Logger log = Logger.getLogger(PaasProjectMainInterceptor.class.getName());
}